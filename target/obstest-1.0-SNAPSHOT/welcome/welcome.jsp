<%-- 
    Document   : welcome
    Created on : May 27, 2023, 10:25:01 AM
    Author     : user
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
        
        <script src="popupdata.js"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h3>Welcome ${userId}</h3>
                </div>
                <div class="col">
                    <a href="login.jsp">Logout</a>
                </div>
            </div>
            <div class="row">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Department</th>
                            <th scope="col">Student ID</th>
                            <th scope="col">Marks</th>
                            <th scope="col">Pass %</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="ds" items="${listDepStd}">
                            <c:forEach var="s" items="${ds.listStudent}" varStatus="status">
                                <tr>
                                    <c:if test="${status.first}">
                                        <td rowspan="${ds.countStudent}"><c:out value="${ds.department}"/></td>
                                    </c:if>
                                        <td>
                                            <input type="submit" id="std${s.studentID}" value="${s.studentName}"/>
                                        </td>
                                    <td><c:out value="${s.mark}"/></td>
                                    <c:if test="${status.first}">
                                        <td rowspan="${ds.countStudent}"><c:out value="${ds.passPercentage}"/></td>
                                    </c:if>    
                                </tr>
                            </c:forEach>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
