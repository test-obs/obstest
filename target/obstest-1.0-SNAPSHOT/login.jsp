<%-- 
    Document   : login
    Created on : May 27, 2023, 9:23:09 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    
        <!-- Including validation.js jQuery Script -->
        <script src="validation.js"></script>
    </head>
    <body>
        <form action="login" method="post">
            <div class="container-md">
                <div class="row">
                    <div class="col-sm">
                        <label for="username"><b>User ID</b></label>
                    </div>
                    <div class="col-sm">
                        <input class="form-control" type="text" placeholder="Please enter your User Id" name="userId" id = "userId" required>
                    </div>
                </div>
                <div class="row">
                    <h5 id="usercheck" style="color: red;">
                        **Username is missing
                    </h5>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <label for="password"><b>Password</b></label>
                    </div>
                    <div class="col-sm">
                        <input class="form-control" type="password" placeholder="Please enter Password" name="password" id="password" required>
                    </div>
                </div>
                <div class="row">
                    <h5 id="passcheck" style="color: red;">
                        **Please Fill the password
                    </h5>
                </div>
                <div class="row">
                    <div class="col-sm"></div>
                    <div class="col-md">
                        <button type="submit">Login</button>
                    </div>
                </div>
            </div>
        </form>
        
    </body>
</html>
