/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.obstest.dao;

import com.mycompany.obstest.model.DepartmentStudent;
import com.mycompany.obstest.model.Student;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class DepartmentStudentDao {
    public List<DepartmentStudent> selectAllMarks(){
        List<DepartmentStudent> listDepartmentStudent = new ArrayList<>();
        //Dep 1
        List<Student> listStdDep1 = new ArrayList<>();
        Student s1 = new Student("S1","S1", 35);
        listStdDep1.add(s1);
        Student s2 = new Student("S2","S2", 70);
        listStdDep1.add(s2);
        Student s3 = new Student("S3","S3", 60);
        listStdDep1.add(s3);
        Student s4 = new Student("S4","S4", 90);
        listStdDep1.add(s4);
        int countDep1 = listStdDep1.size();
        double prcDep1 = calculatePassDepartment(countDep1, listStdDep1);
        DepartmentStudent dep1 = new DepartmentStudent("Dep 1", listStdDep1, countDep1, prcDep1);
        listDepartmentStudent.add(dep1);
        
        //Dep 2
        List<Student> listStdDep2 = new ArrayList<>();
        Student s5 = new Student("S5","S5", 30);
        listStdDep2.add(s5);
        int countDep2 = listStdDep2.size();
        double prcDep2 = calculatePassDepartment(countDep2, listStdDep2);
        DepartmentStudent dep2 = new DepartmentStudent("Dep 2", listStdDep2, countDep2, prcDep2);
        listDepartmentStudent.add(dep2);
        
        //Dep 3
        List<Student> listStdDep3 = new ArrayList<>();
        Student s6 = new Student("S6","S6", 32);
        listStdDep3.add(s6);
        Student s7 = new Student("S7","S7", 70);
        listStdDep3.add(s7);
        Student s8 = new Student("S8","S8", 20);
        listStdDep3.add(s8);
        int countDep3 = listStdDep3.size();
        double prcDep3 = calculatePassDepartment(countDep3, listStdDep3);
        DepartmentStudent dep3 = new DepartmentStudent("Dep 3", listStdDep3, countDep3, prcDep3);
        listDepartmentStudent.add(dep3);
        
        return listDepartmentStudent;
    }
    
    public double calculatePassDepartment(int countStudent, List<Student> listStd){
        double passPercentage = 0;
        int countPass = 0;
        for(int i=0; i<countStudent; i++){
            if(listStd.get(i).getMark()>=40) {countPass++;}
        }
        passPercentage = (double) countPass / countStudent * 100;
        passPercentage = Math.round(passPercentage * 100.0)/100.0;
        
        return passPercentage;
    }
}
