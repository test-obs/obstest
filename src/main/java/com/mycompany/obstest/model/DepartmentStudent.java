/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.obstest.model;

import java.util.List;

/**
 *
 * @author user
 */
public class DepartmentStudent {
    private String department;
    private List<Student> listStudent;
    private Integer countStudent;
    private Double passPercentage;

    public DepartmentStudent(String department, List<Student> listStudent, Integer countStudent, Double passPercentage) {
        this.department = department;
        this.listStudent = listStudent;
        this.countStudent = countStudent;
        this.passPercentage = passPercentage;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public List<Student> getListStudent() {
        return listStudent;
    }

    public void setListStudent(List<Student> listStudent) {
        this.listStudent = listStudent;
    }

    public Integer getCountStudent() {
        return countStudent;
    }

    public void setCountStudent(Integer countStudent) {
        this.countStudent = countStudent;
    }

    public Double getPassPercentage() {
        return passPercentage;
    }

    public void setPassPercentage(Double passPercentage) {
        this.passPercentage = passPercentage;
    }
    
    
}
