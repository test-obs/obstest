$(document).ready(function (){
    //Validate userID
    $("#usercheck").hide();
    let userIdError = true;
    $("#userId").keyup(function () {
        validateUserId();
    });
 
    function validateUserId() {
        let userIdValue = $("#userId").val();
        if (userIdValue.length == "") {
            $("#usercheck").show();
            userIdError = false;
            return false;
        } else if (userIdValue.length < 3 || userIdValue.length > 10) {
            $("#usercheck").show();
            $("#usercheck").html("**length of user id must be between 3 and 10");
            userIdError = false;
            return false;
        } else {
            $("#usercheck").hide();
        }
    }
    
    //Validate password
    $("#passcheck").hide();
    let passwordError = true;
    $("#password").keyup(function () {
        validatePassword();
    });
    function validatePassword() {
        let passwordValue = $("#password").val();
        if (passwordValue.length == "") {
            $("#passcheck").show();
            passwordError = false;
            return false;
        }
        if (passwordValue.length < 3 || passwordValue.length > 10) {
            $("#passcheck").show();
            $("#passcheck").html(
                "**length of your password must be between 3 and 10"
            );
            $("#passcheck").css("color", "red");
            passwordError = false;
            return false;
        } else {
            $("#passcheck").hide();
        }
    }
    
    // Submit button
    $("#submitbtn").click(function () {
        validateUserId();
        validatePassword();
        if (
            userIdError == true &&
            passwordError == true
        ) {
            return true;
        } else {
            return false;
        }
    });
});